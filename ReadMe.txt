/* ReadMe */

this program unpack all files in DPMX archive.
all required parameters for data extraction/decryption will be automatically calculated and selected.

[mode]
--extract / -e : extract all files in archive
--list / -l : list all files in archive
--find-param: find and show parameters

[option for input]
--input / -i [file] : specify archive

[option for data decryption]
** you need not to specify following parameters normally, but you can if you want
** if you set these parameters manually, automatically selected value will be overriden
--offset: specify offset to DPMX header. normally, this value will be zero for data.dpm. if archive embeded in exe, then this value will be greater than zero.
--key: specify archive key. this is for embeded archive
--sum: specify sum that is (size of start.ax)
--salt : salt for data stream. if you can't unpack archive, try this option. eg) --salt 55aa or --salt a55a or --salt 5aa5
--v3-engine : use v3 engine for data decryption.

[option for output]
--dest [dest] : specify destination directory for extraction. dest must be ended with separator. ex) c:\destination_directory\


** how to find offset , key and sum for embeded archive **

open [target.exe] with hex editor.
search string "DPMX" from the end of [target.exe]
found address is the offset.

then, calculate value (offset - 0x10000). ex) if offset is 0x37000 , then 0x37000 - 0x10000 = 0x27000 (= 159744 in decimal)
search that string. in above example, search "159744"
found address + 0x17h is the address of key.
if found address is 0x34149 then 0x34149 + 0x17 = 0x34160 is the address of key.
read 4 bytes from that address. this is a value for key. (assumes that little-endian)

sum is (offset of start.ax + size of start.ax)
