#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <cstdint>
#include <cassert>

typedef struct tagDPMX_STORED_FILE_INFO
{
	uint8_t file_name[16];
	uint32_t unknown;
	uint32_t key;
	uint32_t offset;
	uint32_t size;
} DPMX_STORED_FILE_INFO;

class DPMXFileEntry
{
public:
	DPMXFileEntry() : number_of_entries(0) {};
	virtual ~DPMXFileEntry() {};
	unsigned int read_entries_from_file(std::ifstream& archive_file,unsigned int max_entries,unsigned int offset = 0);
	unsigned int get_number_of_entries(void) { return this->number_of_entries; };
	std::vector<DPMX_STORED_FILE_INFO>& get_entries(void) { return this->file_entries; };
private:
	std::vector<DPMX_STORED_FILE_INFO> file_entries;
	unsigned int number_of_entries;
};

unsigned int DPMXFileEntry::read_entries_from_file(std::ifstream& archive_file,unsigned int max_entries,unsigned int offset)
{
	unsigned int entry_count = 0;
	DPMX_STORED_FILE_INFO file_info;

	this->file_entries.clear();

	if ((offset + 16) != archive_file.tellg())
	{
		archive_file.seekg(offset + 16,archive_file.beg);
	}
	for(unsigned int i = 0;i < max_entries;++i)
	{
		archive_file.read((char*)&file_info,32);
		file_info.file_name[15] = file_info.file_name[14] = 0; // truncate file name
		this->file_entries.push_back(file_info);
		++entry_count;
	}
	std::sort(
		this->file_entries.begin(),
		this->file_entries.end(),
		[](DPMX_STORED_FILE_INFO &l,DPMX_STORED_FILE_INFO &r) -> bool { return (l.offset < r.offset); });
	return entry_count;
}

class DPMXDataStreamDecipher
{
public:
	DPMXDataStreamDecipher() : stream_key(0) , init_val(0) , key1(0xAA) , key2(0) , key3(0x55) , key4(0) {};
	virtual ~DPMXDataStreamDecipher() {};
	virtual unsigned int decipher(unsigned char* o,unsigned char *buffer,unsigned int len);
	virtual std::ostream& decipher(std::ostream& os,unsigned char *buffer,unsigned int len);
	virtual std::istream& decipher(std::ostream& out_stream,std::istream& in_stream,unsigned int len);
	virtual int set_archive_key(unsigned int key = 0,unsigned int sum = 0,unsigned int init_val1 = 0xAA,unsigned int init_val2 = 0x55);
	virtual int set_stream_key(unsigned int strm_key = 0,unsigned int init_val1 = 0x55,unsigned int init_val2 = 0xAA);
	virtual int reset_all(unsigned char v = 0,unsigned int k1 = 0xAA,unsigned int k2 = 0,unsigned int k3 = 0x55,unsigned int k4 = 0);
	virtual int reset_state(unsigned char v = 0);
protected:
	unsigned int stream_key;
	unsigned int key1;
	unsigned int key2;
	unsigned int key3;
	unsigned int key4;
	unsigned char init_val;
};

unsigned int DPMXDataStreamDecipher::decipher(unsigned char* o,unsigned char *buffer,unsigned int len)
{
	unsigned char c = 0;
	unsigned int nBytes = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	for(unsigned int i = 0;i < len;++i)
	{
		c = *(buffer + i);
		if (stream_key)
		{
			c = init_val += (((c - composite_key2) & 0xff) ^ composite_key1);
		}
		*o++ = c;
		nBytes++;
	}
	return nBytes;
}

std::ostream& DPMXDataStreamDecipher::decipher(std::ostream& os,unsigned char *buffer,unsigned int len)
{
	unsigned char c = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	for(unsigned int i = 0;i < len;++i)
	{
		c = *(buffer + i);
		if (stream_key)
		{
			c = init_val += ((c - composite_key2) ^ composite_key1);
		}
		os << c;
	}
	return os;
}

std::istream& DPMXDataStreamDecipher::decipher(std::ostream& out_stream,std::istream& in_stream,unsigned int len)
{
	unsigned char c = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	for(unsigned int i = 0;i < len;++i)
	{
		in_stream.read((char*)&c,1);
		if (stream_key)
		{
			c = init_val += (((c - composite_key2) & 0xff) ^ composite_key1);
		}
		out_stream << c;
	}
	return in_stream;
}

int DPMXDataStreamDecipher::set_stream_key(unsigned int strm_key,unsigned int init_val1,unsigned int init_val2)
{
	this->stream_key = strm_key;
	this->key2 = ((strm_key & 0xff) + init_val1) ^ ((strm_key >> 16) & 0xff);
	this->key4 = (((strm_key >> 8) & 0xff) + init_val2) ^ ((strm_key >> 24) & 0xff);
	return 0;
}

int DPMXDataStreamDecipher::set_archive_key(unsigned int key,unsigned int sum,unsigned int init_val1,unsigned int init_val2)
{
	if (key == 0xffffffff)
	{
		this->key1 = init_val1;
		this->key3 = init_val2;
	}
	else
	{
		this->key1 = (((((key >> 16) & 0xff) * (key & 0xff)) / 3) ^ sum) & 0xff;
		this->key3 = (((((key >> 24) & 0xff) * ((key >> 8) & 0xff)) / 5) ^ sum ^ 0xffffffaa) & 0xff;
	}
	return 0;
}

int DPMXDataStreamDecipher::reset_all(unsigned char v,unsigned int k1,unsigned int k2,unsigned int k3,unsigned int k4)
{
	key1 = k1;
	key2 = k2;
	key3 = k3;
	key4 = k4;
	init_val = v;
	return 0;
}

int DPMXDataStreamDecipher::reset_state(unsigned char v)
{
	this->init_val = v;
	return 0;
}

class DPMXDataStreamDecipherV3 : public DPMXDataStreamDecipher
{
public:
	DPMXDataStreamDecipherV3() : DPMXDataStreamDecipher() {};
	virtual ~DPMXDataStreamDecipherV3() {};
	virtual unsigned int decipher(unsigned char* o,unsigned char* buffer,unsigned int len);
	virtual std::ostream& decipher(std::ostream& os,unsigned char *buffer,unsigned int len);
	virtual std::istream& decipher(std::ostream& out_stream,std::istream& in_stream,unsigned int len);
};

unsigned int DPMXDataStreamDecipherV3::decipher(unsigned char *o,unsigned char *buffer,unsigned int len)
{
	unsigned char c = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	unsigned int nBytes = 0;
	for(unsigned int i = 0;i < len;++i)
	{
		c = *(buffer + i);
		if (stream_key)
		{
			c = init_val += (((c ^ composite_key1) - composite_key2) & 0xff);
		}
		*o++ = c;
		nBytes++;
	}
	return nBytes;
}

std::ostream& DPMXDataStreamDecipherV3::decipher(std::ostream& os,unsigned char *buffer,unsigned int len)
{
	unsigned char c = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	for(unsigned int i = 0;i < len;++i)
	{
		c = *(buffer + i);
		if (stream_key)
		{
			c = init_val += (((c ^ composite_key1) - composite_key2) & 0xff);
		}
		os << c;
	}
	return os;
}

std::istream& DPMXDataStreamDecipherV3::decipher(std::ostream& out_stream,std::istream& in_stream,unsigned int len)
{
	unsigned char c = 0;
	unsigned char composite_key1 = (this->key1 + this->key2) & 0xff;
	unsigned char composite_key2 = (this->key3 + this->key4) & 0xff;
	for(unsigned int i = 0;i < len;++i)
	{
		in_stream.read((char*)&c,1);
		if (stream_key)
		{
			c = init_val += (((c ^ composite_key1) - composite_key2) & 0xff);
		}
		out_stream << c;
	}
	return in_stream;
}

typedef struct tagDPMXPARAM{
	uint32_t offset;
	uint32_t key;
	uint32_t sum;
	std::vector<std::pair<unsigned int,unsigned int> > possible_salt;
} DPMXPARAM;

class DPMXArchive
{
public:
	DPMXArchive() : number_of_files(0) , header_size(0) , DPMX_offset(0) ,archiveParam({0}) {};
	virtual ~DPMXArchive() {};
	int open(std::string& file_path);
	int close(void);
	bool has_valid_header(void);
	unsigned int update_file_entries(void);
	std::vector<DPMX_STORED_FILE_INFO>& get_file_entries(void) { return this->file_entry.get_entries(); };
	std::ifstream& get_data_stream(void) { return this->archive_file; };
	std::ifstream& get_nth_item_stream(unsigned int n) {
		if (n == 0) n = 1;
		if (n > this->number_of_files) n = this->number_of_files;
		if (this->number_of_files)
		{
			this->archive_file.seekg(this->DPMX_offset + 16 + (32 * (this->number_of_files)) + this->file_entry.get_entries()[n - 1].offset,this->archive_file.beg);
		}
		return this->archive_file;
	};
	unsigned int get_number_of_files(void) { return this->number_of_files; };
	int set_DPMX_offset(unsigned int ofs) { this->DPMX_offset = ofs; this->update_file_entries(); return 0; };
	DPMXPARAM get_param(void) { return this->archiveParam; };

private:
	DPMXPARAM find_set_param(void);
	uint32_t find_offset(void);
	uint32_t find_key(uint32_t offset);
	uint32_t find_sum(uint32_t archive_key = 0);
	std::vector<std::pair<unsigned int,unsigned int>> list_salt_by_KPA(uint8_t *data,DPMX_STORED_FILE_INFO& info,uint32_t archiveKey,uint32_t sum);
	std::vector<std::pair<unsigned int,unsigned int>> get_all_possible_salt(uint8_t* data,uint32_t data_size,DPMX_STORED_FILE_INFO& info,uint32_t key = 0,uint32_t sum = 0);

	const std::string hdr_magic = "DPMX";
	unsigned int number_of_files;
	DPMXFileEntry file_entry;
	unsigned int header_size;
	unsigned int DPMX_offset; // Offset to DPMX archive header from begining of the file.
	DPMXPARAM archiveParam;

	std::ifstream archive_file;
};

int DPMXArchive::open(std::string &file_path)
{
	if (this->archive_file.is_open())
		return -1;
	this->archive_file.open(file_path.c_str(),std::ifstream::binary);
	this->find_set_param();
	return 0;
}

int DPMXArchive::close(void)
{
	if (this->archive_file.is_open())
	{
		this->archive_file.close();
		return 0;
	}
	else return -1;
}

bool DPMXArchive::has_valid_header(void)
{
	uint64_t file_size = 0;
	char hdr[16] = {0};

	this->archive_file.seekg(0,this->archive_file.end);
	file_size = this->archive_file.tellg();

	this->archive_file.seekg(this->DPMX_offset,this->archive_file.beg);
	this->archive_file.read(hdr,16);

	bool magic_validity = hdr_magic.compare(0,4,(char*)hdr,4) == 0;
	this->header_size = *((uint32_t*)&hdr[4]);
	this->number_of_files = *((uint32_t*)&hdr[8]);
	return (magic_validity && (this->header_size == (this->number_of_files * 32 + 16)) && (file_size >= (this->header_size + this->DPMX_offset)));
}

unsigned int DPMXArchive::update_file_entries(void)
{
	unsigned int entry_count = 0;
	if (this->has_valid_header())
	{
		entry_count = this->file_entry.read_entries_from_file(this->archive_file,this->number_of_files,this->DPMX_offset);
		assert(entry_count == this->number_of_files);
	}
	this->number_of_files = entry_count;
	return entry_count;
}

uint32_t DPMXArchive::find_offset(void)
{
	uint32_t offset = 0;
	uint32_t file_size = 0;
	char data[4] = {0};

	this->archive_file.seekg(0,this->archive_file.end);
	file_size = this->archive_file.tellg();
	for(;offset < (file_size - 4);offset++)
	{
		this->archive_file.seekg(offset,this->archive_file.beg);
		this->archive_file.read(data,4);
		if (data[0] == 'D' && data[1] == 'P' && data[2] == 'M' && data[3] == 'X' && (this->DPMX_offset = offset , this->has_valid_header()))
		{
			this->update_file_entries();
			break;
		}
	}
	return offset;
}

uint32_t DPMXArchive::find_key(uint32_t offset)
{
	if (offset == 0) return 0xffffffff;
	std::stringstream offset_str;
	offset_str << std::dec << (offset - 0x10000);
	for(uint32_t key_offset = 0;key_offset < offset;key_offset++)
	{
		char s[32] = {0};
		this->archive_file.seekg(key_offset,this->archive_file.beg);
		this->archive_file.read(s,offset_str.str().length());
		if (offset_str.str().compare(s) == 0)
		{
			uint32_t key = 0;
			this->archive_file.seekg(key_offset + 0x17,this->archive_file.beg);
			this->archive_file.read((char*)&key,4);
			return key;
		}
	}
	return 0xffffffff;
}

uint32_t DPMXArchive::find_sum(uint32_t archive_key)
{
	unsigned int num_entries = this->get_number_of_files();
	uint32_t sum = 0;
	if (archive_key == 0) return 0; // if archive_key is zero, sum might be zero.
	for(unsigned int i = 0;i < num_entries;i++)
	{
		if (_stricmp((char*)this->get_file_entries()[i].file_name,"start.ax") == 0)
		{
			sum = this->get_file_entries()[i].size;
			break;
		}
	}
	return sum;
}

std::vector<std::pair<unsigned int, unsigned int> > DPMXArchive::list_salt_by_KPA(uint8_t *data, DPMX_STORED_FILE_INFO& info,uint32_t archiveKey,uint32_t sum)
{
	std::vector<std::pair<unsigned int,unsigned int> > saltList;

	unsigned char known_ax_hdr[3] = {'H','S','P'};
	unsigned char known_bmp_hdr[2] = {'B','M'};
	unsigned char known_jpg_hdr[2] = {0xff,0xd8}; // SOI marker
	unsigned char known_png_hdr[16] = {0x89,'P','N','G',0x0d,0x0a,0x1a,0x0a,0,0,0,13,'I','H','D','R'};
//	unsigned char known_wav_hdr[4] = {'R','I','F','F'};
	unsigned char known_wav_fmt[4] = {'W','A','V','E'};

	std::map<
		std::string, /* ext */
			std::pair<
				int, /* header offset */
				std::pair<unsigned char*,int> /* header , header size */
			>
	> knownHeaderList;
	knownHeaderList.insert({".ax",{0,{known_ax_hdr,sizeof(known_ax_hdr)}}});
	knownHeaderList.insert({".bmp",{0,{known_bmp_hdr,sizeof(known_bmp_hdr)}}});
	knownHeaderList.insert({".jpg",{0,{known_jpg_hdr,sizeof(known_jpg_hdr)}}});
	knownHeaderList.insert({".jpeg",{0,{known_jpg_hdr,sizeof(known_jpg_hdr)}}});
	knownHeaderList.insert({".png",{0,{known_png_hdr,sizeof(known_png_hdr)}}});
	knownHeaderList.insert({".wav",{8,{known_wav_fmt,sizeof(known_wav_fmt)}}});

	auto recover_salt_from_composite_key = [](uint32_t archiveKey,uint32_t archiveSum,uint32_t streamKey,unsigned char compositeKey1,unsigned char compositeKey2) -> unsigned int {
		unsigned int k1,k3;
		if (archiveKey == 0xffffffff)
		{
			// use default value if archiveKey is -1
			k1 = 0xaa;
			k3 = 0x55;
		}
		else
		{
			k1 = (((((archiveKey >> 16) & 0xff) * (archiveKey & 0xff)) / 3) ^ archiveSum) & 0xff;
			k3 = (((((archiveKey >> 24) & 0xff) * ((archiveKey >> 8) & 0xff)) / 5) ^ archiveSum ^ 0xffffffaa) & 0xff;
		}
		unsigned char saltHigh = 0,saltLow = 0;
		saltHigh = ((compositeKey1 - k1) ^ ((streamKey >> 16) & 0xff)) - (streamKey & 0xff);
		saltLow = ((compositeKey2 - k3) ^ ((streamKey >> 24) & 0xff)) - ((streamKey >> 8) & 0xff);
		return ((unsigned int)saltHigh << 8)|saltLow;
	};

	char *ext = strrchr((char*)info.file_name,'.');

	if (ext && knownHeaderList.end() != knownHeaderList.find(std::string(ext)))
	{
		unsigned int key1 = 0, key2 = 0;
		for(key2 = 0;key2 <= 0xff;key2++)
		{
			unsigned char *known_hdr = knownHeaderList[std::string(ext)].second.first;
			int hdr_size = knownHeaderList[std::string(ext)].second.second;
			int hdr_offset = knownHeaderList[std::string(ext)].first;
			if (info.size >= (hdr_offset + hdr_size))
			{
				bool validKeyForV2 = true;
				bool validKeyForV3 = true;
				for(int i = hdr_size - 1;i > 0;i--)
				{
					unsigned char pn = (*(known_hdr + i) - *(known_hdr + i - 1));
					unsigned char p1 = (*(known_hdr + i - 1) - (i == 1 ? 0 : *(known_hdr + i - 2)));
					unsigned char xn = *(data + hdr_offset + i) - key2;
					unsigned char x1 = *(data + hdr_offset + i - 1) - key2;
					unsigned char qn = pn + key2;
					unsigned char q1 = p1 + key2;
					unsigned char yn = xn + key2;
					unsigned char y1 = x1 + key2;
					if ((p1 ^ pn ^ xn) != x1)
					{
						validKeyForV2 = false;
					}
					else if ((q1 ^ qn ^ yn) != y1)
					{
						validKeyForV3 = false;
					}
					if (!validKeyForV2 && !validKeyForV3)
					{
						break;
					}
				}
				if (validKeyForV2 || validKeyForV3)
				{
					if (validKeyForV2)
						key1 = ((*(data + hdr_offset) - key2) & 0xff) ^ *known_hdr;
					else
						key1 = (*(data + hdr_offset) & 0xff) ^ ((*known_hdr + key2) & 0xff);
//					std::cout << "found key pair: " << std::hex << key1 << " , " << key2 << " for " << (validKeyForV2 ? "V2" : "V3") << std::endl;
					if (validKeyForV2)
						saltList.push_back({2,recover_salt_from_composite_key(archiveKey,sum,info.key,key1,key2)});
					else
						saltList.push_back({3,recover_salt_from_composite_key(archiveKey,sum,info.key,key1,key2)});
				}
			}
		}
	}
	return saltList;
}

std::vector<std::pair<unsigned int, unsigned int> > DPMXArchive::get_all_possible_salt(uint8_t *data, uint32_t data_size, DPMX_STORED_FILE_INFO& info,uint32_t key,uint32_t sum)
{
	std::vector<std::pair<unsigned int,unsigned int>> salt_list;
	char known_ext_ax[] = ".ax";
	char known_ext_png[] = ".png";
	char known_ext_jpg1[] = ".jpg";
	char known_ext_jpg2[] = ".jpeg";
	char known_ext_bmp[] = ".bmp";
	char known_ext_wav[] = ".wav";
	auto get_req_size = [&](char* ext) -> int {
		if (_stricmp(ext,known_ext_ax) == 0)
		{
			return 4;
		}
		else if (_stricmp(ext,known_ext_png) == 0)
		{
			return 16;
		}
		else if (_stricmp(ext,known_ext_jpg1) == 0 || _stricmp(ext,known_ext_jpg2) == 0)
		{
			return 2;
		}
		else if (_stricmp(ext,known_ext_bmp) == 0)
		{
			return 18;
		}
		else if (_stricmp(ext,known_ext_wav) == 0)
		{
			return 16;
		}
		return 0;
	};
	auto hdr_check = [&](char* ext,char* data,int data_len,unsigned int file_size) -> bool {
		if (ext == nullptr) return true;
		int hdr_size = get_req_size(ext);
		if (data_len < hdr_size) return false;
		if (_stricmp(ext,known_ext_ax) == 0)
		{
			return data[0] == 'H' && data[1] == 'S' && data[2] == 'P' && (data[3] >= 0x30 && 0x39 >= data[3]);
		}
		else if (_stricmp(ext,known_ext_png) == 0)
		{
			unsigned char png_hdr[16] = {0x89,'P','N','G',0x0d,0x0a,0x1a,0x0a,0,0,0,13,'I','H','D','R'};
			return memcmp(png_hdr,data,hdr_size) == 0;
		}
		else if (_stricmp(ext,known_ext_jpg1) == 0 || _stricmp(ext,known_ext_jpg2) == 0)
		{
			unsigned char jpg_hdr[2] = {0xff,0xd8};
			return memcmp(jpg_hdr,data,hdr_size) == 0;
		}
		else if (_stricmp(ext,known_ext_bmp) == 0)
		{
			bool isSigValid = data[0] == 'B' && data[1] == 'M';
			bool reservedCheck = *((uint32_t*)&data[6]) == 0;
			bool isInfoHeaderSizeValid = *((uint32_t*)&data[14]) == 12 /* OS/2 BMP */ || *((uint32_t*)&data[14]) == 40; /* windows bmp */
			bool isFileSizeLargeEnough = *((uint32_t*)&data[2]) == file_size;
			return isSigValid && reservedCheck && isInfoHeaderSizeValid && isFileSizeLargeEnough;
		}
		else if (_stricmp(ext,known_ext_wav) == 0)
		{
			char riffSig[4] = {'R','I','F','F'};
			char fmtSig[8] = {'W','A','V','E','f','m','t',' '};
			bool sizeCheck = *((uint32_t*)&data[4]) + 8 == file_size;
			return memcmp(riffSig,data,4) == 0 && memcmp(fmtSig,data + 8,8) == 0 && sizeCheck;
		}
		return false;
	};
	std::vector<std::pair<unsigned int,unsigned int>> v = this->list_salt_by_KPA(data,info,key,sum);
	DPMXDataStreamDecipher decipherV2;
	DPMXDataStreamDecipherV3 decipherV3;
	decipherV2.set_archive_key(key,sum);
	decipherV3.set_archive_key(key,sum);

	for(auto& e : v)
	{
		unsigned int salt = e.second;
		char *ext = strrchr((char*)info.file_name,'.');
		int req_size = get_req_size(ext);
		unsigned char *decryptedV2 = new unsigned char [req_size < 0 ? data_size : req_size];
		unsigned char *decryptedV3 = new unsigned char [req_size < 0 ? data_size : req_size];
		decipherV2.reset_state();
		decipherV3.reset_state();
		decipherV2.set_stream_key(info.key,salt >> 8,salt & 0xff);
		decipherV3.set_stream_key(info.key,salt >> 8,salt & 0xff);
		decipherV2.decipher(decryptedV2,data,req_size < 0 ? data_size : req_size);
		decipherV3.decipher(decryptedV3,data,req_size < 0 ? data_size : req_size);
		if (hdr_check(ext,(char*)decryptedV2,req_size < 0 ? data_size : req_size,info.size))
		{
			salt_list.push_back(std::pair<unsigned int,unsigned int>(2,salt));
		}
		else if (hdr_check(ext,(char*)decryptedV3,req_size < 0 ? data_size : req_size,info.size))
		{
			salt_list.push_back(std::pair<unsigned int,unsigned int>(3,salt));
		}
		delete [] decryptedV2;
		delete [] decryptedV3;
	}

	return salt_list;
}

DPMXPARAM DPMXArchive::find_set_param(void)
{
//	DPMXPARAM param = {0};
	this->archiveParam.offset = this->find_offset();
	this->archiveParam.key = this->find_key(this->archiveParam.offset);
	this->archiveParam.sum = this->find_sum(this->archiveParam.key);
	unsigned int num_entries = this->get_number_of_files();

	for(unsigned int i = 0;i < num_entries;i++)
	{
		char *data = new char[this->get_file_entries()[0].size]();
		this->get_nth_item_stream(i + 1).read(data,this->get_file_entries()[i].size);
		this->archiveParam.possible_salt = this->get_all_possible_salt((uint8_t*)data,this->get_file_entries()[0].size,this->get_file_entries()[0],this->archiveParam.key,this->archiveParam.sum);
		delete [] data;
//		std::cout << "possible salts" << std::endl;
//		for(auto e : param.possible_salt)
//		{
//			std::cout << std::hex << e.second << std::endl;
//		}
		if (this->archiveParam.possible_salt.size()) break;
	}
	return this->archiveParam;
}

int main(int argc,char **argv)
{
	DPMXArchive archive;
	std::string archive_path;
	std::string dest_dir;
	unsigned int custom_offset = 0;
	unsigned int custom_key = 0;
	unsigned int custom_sum = 0;
	unsigned int custom_salt = 0;
	unsigned int custom_engine_ver = 2; // default value is 2 for hsp2.61
	bool useCustomOffset = false,useCustomKey = false,useCustomSum = false,useCustomSalt = false,useCustomEngineVer = false;
	int mode = 0;

	// parse command line
	for(int i = 1;i < argc;++i)
	{
		if (_stricmp(argv[i],"--input") == 0 || _stricmp(argv[i],"-i") == 0)
		{
			if ((i + 1) < argc)
			{
				archive_path = argv[i + 1];
			}
			++i;
		}
		else if (_stricmp(argv[i],"--offset") == 0)
		{
			if ((i + 1) < argc)
			{
				useCustomOffset = true;
				custom_offset = strtoul(argv[i + 1],nullptr,16);
			}
			++i;
		}
		else if (_stricmp(argv[i],"--key") == 0)
		{
			if ((i + 1) < argc)
			{
				useCustomKey = true;
				custom_key = strtoul(argv[i + 1],nullptr,16);
			}
			++i;
		}
		else if (_stricmp(argv[i],"--sum") == 0)
		{
			if ((i + 1) < argc)
			{
				useCustomSum = true;
				custom_sum = strtoul(argv[i + 1],nullptr,16);
			}
			++i;
		}
		else if (_stricmp(argv[i],"--salt") == 0)
		{
			if ((i + 1) < argc)
			{
				useCustomSalt = true;
				custom_salt = strtoul(argv[i + 1],nullptr,16);
			}
			++i;
		}
		else if (_stricmp(argv[i],"--v3-engine") == 0)
		{
			useCustomEngineVer = true;
			custom_engine_ver = 3;
		}
		else if (_stricmp(argv[i],"--list") == 0 || _stricmp(argv[i],"-l") == 0)
		{
			// listing mode
			mode = 0;
		}
		else if (_stricmp(argv[i],"--extract") == 0 || _stricmp(argv[i],"-e") == 0)
		{
			mode = 1;
		}
		else if (_stricmp(argv[i],"--find-param") == 0)
		{
			mode = 2;
		}
		else if (_stricmp(argv[i],"--dest") == 0)
		{
			if ((i + 1) < argc)
			{
				dest_dir = argv[i + 1];
			}
			++i;
		}
	}

	if (archive_path.length())
	{
		DPMXPARAM param;
		DPMXDataStreamDecipher v2engine;
		DPMXDataStreamDecipherV3 v3engine;
		DPMXDataStreamDecipher* decipher = nullptr;
		unsigned int default_salt_v2 = 0x55aa;
		unsigned int default_salt_v3 = 0x5aa5;
		unsigned int selected_salt = 0;
		unsigned int selected_engine_ver = 2;
		unsigned int salt = 0;
		unsigned int engineVer = 2;
		unsigned int num_entries = 0;

		// open archive
		archive.open(archive_path);

		// finding parameters
		std::cout << "finding parameters..." << std::endl;
		param = archive.get_param();
//		std::cout << "found parameters" << std::endl;
//		std::cout << "offset = " << std::hex << param.offset << std::endl;
//		std::cout << "key = " << std::hex << param.key << std::endl;
//		std::cout << "sum = " << std::hex << param.sum << std::endl;

		// setting parameters
		if (useCustomOffset)
			archive.set_DPMX_offset(custom_offset);
		num_entries = archive.get_number_of_files();
		v2engine.set_archive_key(useCustomKey ? custom_key : param.key,useCustomSum ? custom_sum : param.sum);
		v3engine.set_archive_key(useCustomKey ? custom_key : param.key,useCustomSum ? custom_sum : param.sum);

		if (num_entries) selected_salt = param.possible_salt.size() ? param.possible_salt[0].second : 0;
//		std::cout << "automatically selected salt: " << std::hex << selected_salt << std::endl;

		std::cout << "selecting parameters..." << std::endl;
		if (!param.possible_salt.size()) std::cout << "no salt found" << std::endl;
		// select suitable, wellknown salt
		for(auto e : param.possible_salt)
		{
			if (e.second == 0x55aa)
			{
				selected_salt = 0x55aa;
				selected_engine_ver = e.first;
			}
			else if (e.second == 0x5aa5)
			{
				selected_salt = 0x5aa5;
				selected_engine_ver = e.first;
			}
			else if (e.second == 0xaa55)
			{
				selected_salt = 0xaa55;
				selected_engine_ver = e.first;
			}
			else if (e.second == 0xa55a)
			{
				selected_salt = 0xa55a;
				selected_engine_ver = e.first;
			}
		}
		salt = useCustomSalt ? custom_salt : (selected_salt ? selected_salt : (selected_engine_ver == 2 ? default_salt_v2 : default_salt_v3));
		engineVer = useCustomEngineVer ? custom_engine_ver : selected_engine_ver;
		if (custom_engine_ver != selected_engine_ver)
		{
			if (!useCustomSalt)
			{
				salt = custom_engine_ver == 2 ? default_salt_v2 : default_salt_v3;
				for(auto e : param.possible_salt)
				{
					if (e.first == custom_engine_ver)
					{
						salt = e.second;
						break;
					}
				}
			}
		}
		switch (engineVer)
		{
		case 2:
			decipher = &v2engine;
			break;
		case 3:
			decipher = &v3engine;
			break;
		}
		std::cout << "use engine version: " << engineVer << std::endl;
		std::cout << "use salt: " << std::hex << salt << std::endl;
		switch (mode)
		{
		case 0:
			// listing mode
			{
				for(unsigned int i = 0;i < num_entries;++i)
				{
					std::cout << std::dec << (i + 1) << " / " << num_entries << '\t';
					std::cout << (char*)archive.get_file_entries()[i].file_name;
					std::cout << " " << archive.get_file_entries()[i].size << " bytes" << std::endl;
				}
			}
			break;
		case 1:
			{
				unsigned int data_size = 0;
				unsigned char *file_data = nullptr;


				std::cout << "Extracting ..." << std::endl << std::endl;

				for(unsigned int i = 0;i < num_entries;++i)
				{
					std::string dest_path = dest_dir + (char*)archive.get_file_entries()[i].file_name;
					std::ofstream out_file(dest_path.c_str(),std::ofstream::binary);

					std::cout << std::dec << (i + 1) << " / " << num_entries << '\t';
					std::cout << (char*)archive.get_file_entries()[i].file_name << std::endl;

					data_size = archive.get_file_entries()[i].size;
					file_data = new unsigned char [data_size];
					archive.get_data_stream().read((char*)file_data,data_size);

					decipher->reset_state();
					decipher->set_stream_key(archive.get_file_entries()[i].key,(salt >> 8) & 0xff,salt & 0xff);
					decipher->decipher(out_file,file_data,data_size);

					out_file.close();
					delete [] file_data;
				}
			}
			break;
		case 2:
			{
				std::cout << "offset = " << std::hex << param.offset << std::endl;
				std::cout << "key = " << std::hex << param.key << std::endl;
				std::cout << "sum = " << std::hex << param.sum << std::endl;
				std::cout << "possible salts =" << std::endl;
				for(auto e : param.possible_salt)
				{
					std::cout << std::hex << e.second << std::endl;
				}
				std::cout << std::endl;
				std::cout << "suggested command line arguments =" << std::endl;
				for(auto e : param.possible_salt)
				{
					std::cout << argv[0] << " --extract -i " << archive_path << " ";
					std::cout << "--offset " << param.offset << " --key " << param.key << " --sum " << param.sum;
					if (e.first == 3) std::cout << " --v3-engine";
					std::cout << " --salt " << e.second << std::endl;
				}
			}
		}
	}
	else
	{
		std::cout << "Usage: " << argv[0] << " [mode] [--input | -i] file [opt]" << std::endl;
		std::cout << "see ReadMe for more information." << std::endl;
	}
	return 0;
}
